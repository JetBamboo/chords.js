/**
* @version: 1.0.0
* @author: Chaos Bamboo
* @copyright: Copyright (c) 2018-2018 Chaos Bamboo. All rights reserved.
* @website: http://www.bamboo.ac.cn/
* @gitlab: https://gitlab.com/JetBamboo/chords.js
*/
;
(function($) {

  'use strict'

  let _canvas = {};  // 画布，可以设置长宽
  let _ctx = {};     // 画笔
  let _area = [];    // 定义品弦区域位置 _area[pin][xian]

  let _padding = [];
  let _color = {
    font: {
      dark: '#222222',
      light: '#ffffff'
    },
    panel: {
      dark: '#519fce',
      light: '#70aeef'
    },
    mark: {
      dark: '#1573a0',
      light: '#3c8dbc'
    },
    line: '#ffffff',
  };

  const _font_family = 'Microsoft YaHei';
  const _line_weight = 2;

  $.fn.extend({

    Chords: function() {

      // Private
      let $this = this;  // jQuery对象
      let _data = {}; // 和弦数据

      let _chord_start = {}; // 和弦谱面左上坐标
      let _chord_end = {};   // 和弦谱面右下坐标
      let _chord_width = 0;  // 和弦谱面宽度
      let _chord_height = 0; // 和弦谱面高度
      let _unit_width = 0;   // 品格宽度
      let _unit_height = 0;  // 品格高度

      const _head_height = 8;

      function _del(i, type) {
        type === 'cross'
          ? _data.cross.splice(i, 1)
          : _data.tap.splice(i, 1);
        _public.clear();
        _public.drawAll();
      }

      const _public = {
        init: function(w = 260, h = 370) {

          // 默认数值
          _padding = [60, 30, 30, 30];
          _color = {
            font: {
              dark: '#222222',
              light: '#ffffff'
            },
            panel: {
              dark: '#519fce',
              light: '#70aeef'
            },
            mark: {
              dark: '#1573a0',
              light: '#3c8dbc'
            },
            line: '#ffffff',
          };
          _data = {
            name: '',
            cross: [], // {id: 0, finger: 0, pin: 0, xian: []}
            tap: [],   // {id: 0, finger: 0, pin: 0, xian: 0}
            ban: [2, 2, 2, 2, 2, 2]
          }
          
          // 获取画布
          _canvas = $this.get(0);
          _ctx = _canvas.getContext('2d');

          // 确定数据
          _canvas.width  = w;
          _canvas.height = h;
          _chord_width  = w - _padding[1] - _padding[3]; // 和弦的宽
          _chord_height = h - _padding[0] - _padding[2] - _head_height - _line_weight; // 和弦的长
          _unit_width   = _chord_width / 5;  // 每个品格的宽
          _unit_height  = _chord_height / 5; // 每个品格的长
          _chord_start.x = _padding[3];      // 品格开始的x坐标
          _chord_end.x = w - _padding[1];    // 品格结束的x坐标
          _chord_start.y = _padding[0] + _head_height + _line_weight; // 品格开始的y坐标
          _chord_end.y = h - _padding[2];    // 品格开始的y坐标

           // 确定品弦位置
           for(let i = 1; i < 6; i++) {
            _area[i] = [];
            for(let j = 1; j < 7; j++) {
              _area[i][j] = {
                x: _chord_start.x + _unit_width * (6 - j), 
                y: _chord_start.y + _unit_height * (i - 0.5),
              };
            }
          }
          this.drawPanel();
        },

        setName: function(name) {
          _data.name = name;
          this.drawName(name);
        },

        drawName: function(name) {
          name = name? name : _data.name;
          let font_size = 40;
          let font_width = font_size * (name.length * 3 / 4);
          let fix_pos = {
            x: _canvas.width / 2 - font_width / 2,
            y: _padding[0] / 2 + font_size / 3
          };

          _ctx.clearRect(0, 0, _canvas.width, _padding[0])
          _ctx.font = `${font_size}px ${_font_family}`;
          _ctx.fillStyle = _color.font.dark;
          _ctx.fillText(name, fix_pos.x, fix_pos.y);
        },

        setBan: function(ban = []) {
          _data.ban = ban;
          this.drawBan(ban);
        },

        drawBan:function(ban) {
          let base = -20;
          let font_size = 30;
          let fix_pos = {
            x: 0,
            y: 0
          };
          _ctx.font = `${font_size}px ${_font_family}`;
          _ctx.fillStyle = _color.font.dark;
          for(let b of _data.ban) {
            base += 40;
            switch(b) {
              case 0:
                _ctx.fillText("o", base, 360);
                break;
              case 1:
                _ctx.fillText("x", base, 360);
                break;
              default:
            }
          }
        },

        drawPanel: function() {
          _ctx.fillStyle = _color.panel.dark;
          _ctx.fillRect(_chord_start.x, _padding[0], _chord_width, _head_height);
          _ctx.fillStyle = _color.panel.light;
          _ctx.fillRect(_chord_start.x, _chord_start.y, _chord_width, _chord_height);
          for(let i = 0; i < 4; i++) {
            let tmp_y = _chord_start.y + (i + 1) * _unit_height;
            _ctx.moveTo(_chord_start.x, tmp_y);
            _ctx.lineTo(_chord_end.x, tmp_y);
            let tmp_x = _chord_start.x + _unit_width * (i + 1);
            _ctx.moveTo(tmp_x, _chord_start.y);
            _ctx.lineTo(tmp_x, _chord_end.y);
          }
          _ctx.strokeStyle = _color.line;
          _ctx.lineWidth = _line_weight;
          _ctx.stroke();     
        },

        addCross: function(pin = 1, xian = [], finger = 1) {
          _data.cross.push({
            id: Math.floor(new Date().getTime() / 1000),
            finger: finger,
            pin: pin,
            xian: xian
          });
          this.drawCross(pin, xian, finger);
        },

        drawCross: function(pin = 1, xian = [], finger = 1) {
          if(xian.length === 0) return;
          let start = _area[pin][xian[xian.length - 1]];
          let end = _area[pin][xian[0]];
          let px = _unit_width * 0.4; // _padding x;
          let py = _unit_height * 0.1; // _padding y;
          _ctx.beginPath();
          _ctx.moveTo(start.x - px, start.y - py);
          _ctx.lineTo(end.x + px, start.y - py);
          _ctx.lineTo(end.x + px, end.y + py);
          _ctx.lineTo(start.x - px, end.y + py);
          _ctx.lineTo(start.x - px, start.y - py);
          _ctx.strokeStyle = _color.mark.dark;
          _ctx.fillStyle = _color.mark.light;
          _ctx.lineWidth = _line_weight;
          _ctx.fill();
          _ctx.stroke();
        },

        addTap: function(pin = 1, xian = 1, finger = 1) {
          _data.tap.push({
            id: Math.floor(new Date().getTime() / 1000),
            finger: finger,
            pin: pin,
            xian: xian
          });
          this.drawTap(pin, xian, finger);
        },

        drawTap: function(pin = 1, xian = 1, finger = 1) {
          let r = 17;
          let font_size = 25;
          let pos = _area[pin][xian];
          let fix_pos = {
            x: pos.x - font_size / 3.3,
            y: pos.y + font_size / 2.7
          }
          _ctx.beginPath();
          _ctx.strokeStyle = _color.mark.dark;
          _ctx.fillStyle = _color.mark.light;
          _ctx.lineWidth = _line_weight;
          _ctx.arc(pos.x, pos.y, r, 0, 2*Math.PI);
          _ctx.fill();
          _ctx.stroke();
          _ctx.font = `${font_size}px ${_font_family}`;
          _ctx.fillStyle = _color.font.light;
          _ctx.fillText(finger, fix_pos.x, fix_pos.y);
        },

        drawAll: function() {
          this.drawPanel();
          this.drawName(_data.name);
          this.drawBan(_data.ban);
          for(let item of _data.cross) {
            this.drawCross(item.pin, item.xian, item.finger);
          }
          for(let item of _data.tap) {
            this.drawTap(item.pin, item.xian, item.finger);
          }
        },

        remove: function(id) {
          for(let i = 0; i < _data.cross.length; i++) {
            if(_data.cross[i].id === id) {
              _del(i, 'cross');
              return;
            }
          }
          for(let i = 0; i < _data.tap.length; i++) {
            if(_data.tap[i].id === id) {
              _del(i, 'tap');
              return;
            }
          }
        },

        clear: function() {
          _ctx.clearRect(0, 0, _canvas.width, _canvas.height);
        },

        setData: function(data) {
          _data = data;
          this.drawAll();
        },

        getData: function() {
          return _data;
        },

        setFontColor: function(dark, light) {
          _color.font = {
            dark: dark, 
            light: light
          };
        },

        setPanelColor: function(dark, light) {
          _color.panel = {
            dark: dark, 
            light: light
          };
        },
        setMarkColor: function(dark, light) {
          _color.mark = {
            dark: dark, 
            light: light
          };
        },

        setStyle: function(font, panel, mark) {
          font  && this.setFontColor(font);
          panel && this.setPanelColor(panel);
          mark  && this.setMarkColor(mark); 
        },
      }

      return _public;
    },

    Lattice: function() {

      let $this = this;

      let _lat_width = 0;
      let _lat_height = 0;
      let _unit_width = 0;
      let _unit_height = 0;
      let _stack = [];

      const pin_num = 12;
      const xian_num = 6;

      function getEventPosition(ev){
        let o = {};
        if (ev.layerX || ev.layerX == 0) {
          o = {x: ev.layerX, y: ev.layerY};
        } else if (ev.offsetX || ev.offsetX == 0) { // Opera
          o = {x: ev.offsetX, y: ev.offsetY};
        }
        return o;
      }

      function up(num, n) {
        let result = 1;
        for(let i = 0; i < n; i++) {
          result *= num;
        }
        return result;
      }
  
      function toLineDistant(p1, p2) {
        return Math.sqrt(up(p1.x-p2.x, 2) + up(p1.y - p2.y, 2));
      }

      function getFontSize(str, family, size) {
        let hash = 'hidden-font-template-' + new Date().getTime();
        $('body').append(`<span id="${hash}" style="font-size:${size}px;font-family:'${family}';">${str}</span>`);
        let target = $(`#${hash}`);
        let rect = target[0].getBoundingClientRect();
        target.remove();
        return {width: rect.width, height: rect.height};
      }

      $this.click(function(e) {
        let pos = getEventPosition(e);
        for(let i = 0; i < xian_num; i++) {
          for(let j = 0; j < pin_num; j++) {
            let dis = toLineDistant(pos, _area[i][j]);
            if(dis <= _unit_width / 2 && dis <= _unit_height / 2 && !_area[i][j].used) {
              _area[i][j].used = true;
              let o = {val: _stack.length+1, pos: _area[i][j], index: {i: i, j: j}}
              _stack.push(o);
              _public.drawTap(o.val, o.pos);
            }
          }
        }
      });

      const _public = {
        init: function() {
          _canvas = $this.get(0);
          _ctx = _canvas.getContext('2d');
          _padding = [40, 10, 40, 10];
          _color = {
            font: {
              dark: '#222222',
              light: '#ffffff'
            },
            panel: {
              dark: '#aaa',
              light: '#70aeef'
            },
            mark: {
              dark: '#1573a0',
              light: '#3c8dbc'
            },
            line: '#ffffff',
          };
          _canvas.width = 970;
          _canvas.height = 300;
      
          _lat_width = _canvas.width - _padding[1] - _padding[3];
          _lat_height = _canvas.height - _padding[0] - _padding[2];
          _unit_width = _lat_width / 12;
          _unit_height = _lat_height / 5;
          _area = [];
          for(let i = 0; i < xian_num; i++) {
            _area[i] = [];
            for(let j = 0; j < pin_num; j++) {
              _area[i][j] = {
                x: _padding[3] + (j + 0.5) * _unit_width, 
                y: _padding[0] + i * _unit_height,
                used: false
              }
            }
          }
          _public.drawPanel();
        },

        drawPanel: function() {
          _ctx.fillStyle = _color.panel.dark;
          _ctx.fillRect(_padding[3], _padding[0], _lat_width, _lat_height);
    
          _ctx.beginPath();
          for(let i = 0; i < xian_num; i++) {
            _ctx.moveTo(_padding[3], _padding[0] + _unit_height * i);
            _ctx.lineTo(_canvas.width - _padding[1], _padding[0] + _unit_height * i);
          }
          _ctx.strokeStyle = _color.line;
          _ctx.lineWidth = 2;
          _ctx.stroke();
    
          _ctx.beginPath();
          for(let i = 0; i <= pin_num; i++) {
            _ctx.moveTo(_padding[3] + _unit_width * i, _padding[0]);
            _ctx.lineTo(_padding[3] + _unit_width * i, _canvas.height - _padding[2]);
          }
          _ctx.strokeStyle = '#fff';
          _ctx.lineWidth = 2;
          _ctx.stroke();
        },

        drawTap: function(val, pos) {
          let r = 17;
          let font_size = 20;
          let _font_family = 'Microsoft YaHei';
          let gap = getFontSize(val, _font_family, font_size);
          let fix_pos = {
            x: pos.x - gap.width / 2,
            y: pos.y + gap.height / 3
          }
          _ctx.beginPath();
          _ctx.strokeStyle = _color.mark.dark;
          _ctx.fillStyle = _color.mark.light;
          _ctx.lineWidth = 2;
          _ctx.arc(pos.x, pos.y, r, 0, 2*Math.PI);
          _ctx.fill();
          _ctx.stroke();
          _ctx.font = `${font_size}px ${_font_family}`;
          _ctx.fillStyle = _color.font.light;
          _ctx.fillText(val, fix_pos.x, fix_pos.y);
        },

        bindUndo: function(jqstr) {
          $(document).on('click', jqstr, function() {
            let o = _stack.pop();
            if(o == null) return;
            _area[o.index.i][o.index.j].used = false;
            _ctx.clearRect(0, 0, _canvas.width, _canvas.height);
            _public.drawPanel();
            for(let tap of _stack) {
              _public.drawTap(tap.val, tap.pos);
            }
          });
        }
      }

      return _public;
    }
  });
})(jQuery);